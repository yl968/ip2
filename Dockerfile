FROM rust:latest as builder
WORKDIR /usr/src/ip2

COPY . .

RUN cargo install --path .

FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/ip2 /usr/local/bin/ip2
CMD ["ip2"]

