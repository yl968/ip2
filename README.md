
# Rust Actix Web Application

This README outlines the steps to create a simple Rust Actix web application and containerize it using Docker for easy deployment and scaling.


## Creating the Actix Web Application

### Step 1: Set Up a New Rust Project

Create a new binary project using Cargo and navigate into the project directory:

```bash
cargo new ip2 --bin
cd ip2
```

### Step 2: Add Actix Web Dependency

Edit the `Cargo.toml` file to include Actix Web as a dependency:

```toml
[dependencies]
actix-web = "4.0"
```

### Step 3: Implement the Web Server

Replace the content of `src/main.rs` with the following code to create a basic HTTP server:

```rust
use actix_web::{web, App, HttpResponse, HttpServer, Responder};

async fn greet() -> impl Responder {
    HttpResponse::Ok().body("individual project 2")
}

async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
            .route("/echo", web::post().to(echo))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
```


## Containerizing with Docker

### Step 1: Create a Dockerfile

In the root of the Rust project, create a `Dockerfile` with the following content:

```Dockerfile
FROM rust:latest as builder
WORKDIR /usr/src/ip2
COPY . .
RUN cargo install --path .

FROM rust:latest
COPY --from=builder /usr/local/cargo/bin/ip2 /usr/local/bin/ip2

EXPOSE 8080
CMD ["ip2"]
```

### Step 2: Build the Docker Image

Build the Docker image from the Dockerfile:

```bash
docker build -t ip2 .
```

### Step 3: Run the Container

Start a Docker container from the image, mapping port 8080:

```bash
docker run -d -p 8080:8080 ip2
```

The Actix web application is now running inside a Docker container and is accessible at `http://localhost:8080`.

### Docker Container Running

Below is a screenshot showing the Docker container running in Docker Desktop:

![Docker Container Running](fac098941d1a963c0fb1cabf77754c2.png)

Below is a screenshot showing the container created after CI/CD pipeline:
![Docker Container Running](5b1aae39ac53320710a6bba86e7427a.png)

Service working: 
![Service](22c0a14ea2c077f0e8339ecb9b7690f.png)

Demo video:
![video](yuanzhe _ ip2 · GitLab - Google Chrome 2024-03-22 15-05-32.mp4)